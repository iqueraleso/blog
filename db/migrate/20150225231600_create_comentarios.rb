class CreateComentarios < ActiveRecord::Migration
  def change
    create_table :comentarios do |t|
      t.integer :articulo_id
      t.text :cuerpo

      t.timestamps
    end
  end
end
